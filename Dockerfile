ARG RUBY_VERSION=3.2.2

FROM ruby:${RUBY_VERSION}-slim-buster

RUN apt-get update && \
    apt-get install build-essential -y --no-install-recommends \
    vim \
    git \
    gnupg2 \
    curl \
    wget \
    nodejs \
    patch \
    ruby-dev \
    zlib1g-dev \
    liblzma-dev \
    libmariadb-dev

RUN gem install bundler

RUN mkdir /app

WORKDIR /app

COPY Gemfile* ./

RUN bundle install

COPY . /app
COPY command.sh /scripts/

RUN chmod a+x /scripts/*.sh
